#!/bin/bash

MAX_SPRITES=7
DEFAULT_POS="South"

rm /tmp/screen.png
SPRITE_NUM=$((1 + RANDOM % $MAX_SPRITES))

if [ ! -z "$2" ]; then SPRITE_NUM=$2; fi

POSITION=$DEFAULT_POS

case $SPRITE_NUM in
  3|5|8) POSITION="center" ;;
  *) POSITION="South" ;;
esac

scrot /tmp/screen.png
convert -blur 0x3 /tmp/screen.png /tmp/screen.png
convert -composite /tmp/screen.png ~/.i3/sprites/sprite$SPRITE_NUM.png -gravity $POSITION -geometry -20x1200 /tmp/screen.png
convert /tmp/screen.png -strokewidth 0 -fill "rgba(0,0,0,0.7)" -draw "rectangle 20,650 330,750" /tmp/screen.png

i3lock \
	-t -i /tmp/screen.png \
	--timepos='x+110:h-70' \
	--datepos='x+43:h-45' \
	--clock --date-align 1 --datestr "Type password to unlock..." \
	--insidecolor=00000000 --ringcolor=44444444 --line-uses-inside \
	--keyhlcolor=1abb9bff --bshlcolor=d23c3dff --separatorcolor=00000000 \
	--insidevercolor=00000000 --insidewrongcolor=00000000 \
	--ringvercolor=1abb9bff --ringwrongcolor=ff0000ff --indpos='x+280:h-70' \
	--radius=20 --ring-width=4 --veriftext='' --wrongtext='' \
	--verifcolor="ffffffff" --timecolor="ffffffff" --datecolor="ffffffff" \
	--time-font="sans-serif" --date-font="sans-serif" --layout-font="sans-serif" --verif-font="sans-serif" --wrong-font="sans-serif" \
	--noinputtext='' --force-clock --pass-media-keys \
	--nofork

if [ ! -z "$1" ]
  then
   if [ $1 = 'suspend' ]; then systemctl suspend; fi
fi
